#!/usr/bin/env python3

from textadventure.main import main

if __name__ == "__main__":
    main()

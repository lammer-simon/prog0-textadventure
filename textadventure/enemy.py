import random

from .entity import Entity
from .json_serialization import serializable_class, Serializable

@serializable_class
class Enemy(Entity):
    def __init__(self, **kwargs):
        min_gold = kwargs.get('min_gold') 
        max_gold = kwargs.get('max_gold')
        if min_gold and max_gold:
            del kwargs['min_gold']
            del kwargs['max_gold']
            self.gold = random.randint(min_gold, max_gold)
        super().__init__(**kwargs)

import math
import textwrap

from .json_serialization import serializable_class, Serializable

@serializable_class
class Entity(Serializable):
    def __init__(self, **kwargs):
        self.name = ""
        self.health = 100
        self.attack = 0
        self.defense = 0
        self.speed = 0
        super().__init__(**kwargs)

    def print_stats(self):
        print(textwrap.dedent(f"""\
            Name: {self.name}
            Attributes:
            
              * Attack: {self.attack}
              * Defense: {self.defense}
              * Speed: {self.speed}
            """))

    def damage(self, entity):
        damage = math.floor(self.attack ** 2 / (self.attack + entity.defense))
        entity.health = max(0, entity.health - damage)
        return damage

    def isalive(self):
        return self.health > 0

    def equip(self, item):
        self.modify_stats(item.passive_influence)

    def use(self, item):
        self.modify_stats(item.active_influence)
        if isinstance(item.gamestates_after_usage, list):
            return item.gamestates_after_usage
        else:
            return bool(item.active_influence)
    
    def unequip(self, item):
        self.modify_stats(item.passive_influence, -1)

    def modify_stats(self, modifications, modifier=1):
        for attribute, amount in modifications.items():
            if hasattr(self, attribute):
                setattr(self, attribute, getattr(self, attribute) + amount * modifier)

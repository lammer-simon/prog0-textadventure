from . import gamestates
from .json_serialization import save_object, load_object
from .item import Item

class Game:
    INITIAL_STATUS_POINTS = 100

    def __init__(self, savefile="", new_game=False, bonus_tasks=False, **kwargs):
        self.savefile         = savefile
        self.bonus_tasks      = bonus_tasks
        self.next_gamestates  = []
        self.items            = self.initial_items()
        self.gamestates       = self.initial_gamestates()
        self.player           = None
        self.load_game(new_game)

        self.items['portalscroll'] = Item(
            name = "Portal scroll",
            description = "teleports you to the village when used; village can be exited to return to the position when used",
            price = 1,
            usages_left = 1,
            gamestates_after_usage = [gamestates.Portal(
                game = self,
                target_gamestates = [self.gamestates['village']]
            )]
        )
        if self.bonus_tasks:
            self.gamestates['druid'].inventory.append(self.items['portalscroll'])
            self.gamestates['blacksmith'].inventory.extend([
                self.items[i] for i in [
                    'dagger',
                    'greatsword',
                    'obsidian-armor'
                ]
            ])

    def load_game(self, new_game):
        if new_game:
            gamestate = gamestates.CharacterCreation(game=self)
            self.next_gamestates.append(gamestate)
        else:
            self.__dict__.update(load_object(self.savefile))
            for gamestate in self.next_gamestates:
                gamestate.game = self

    def run(self):
        while(self.next_gamestates):
            new_states = self.next_gamestates.pop().run()
            if new_states == None:
                break
            self.next_gamestates.extend(new_states)

    def save(self):
        save_object({
            "next_gamestates": self.next_gamestates,
            "player":     self.player
        }, self.savefile)

    def initial_gamestates(self):
        return {
            'death': gamestates.Death(game=self),
            'save': gamestates.Save(game=self),
            'quit': gamestates.Quit(game=self),
            'inventory': gamestates.Inventory(game=self),
            'village': gamestates.Village(game=self),
            'merchant': gamestates.Merchant(game=self),
            'blacksmith': gamestates.Shop(
                game = self,
                name = "blacksmith",
                inventory = [
                    self.items[i] for i in [
                        'helmet',
                        'chest_plate',
                        'sword'
                    ]
                ]),
            'druid': gamestates.Shop(
                game = self,
                name = "druid",
                inventory = [
                    self.items[i] for i in [
                        'potion',
                        'beer',
                        'coffee',
                        'antidote',
                        'milk'
                    ]
                ]),
            'gravedigger': gamestates.Shop(
                game = self,
                name = "gravedigger",
                inventory = []
            )
        }
    
    def initial_items(self):
        return {
            'helmet': Item(
                name = "Helmet",
                description = "+2 defense when held",
                price = 3,
                passive_influence = { "defense": 2 }
            ),
            'chest_plate': Item(
                name = "Chest plate",
                description = "+4 defense when held",
                price = 5,
                passive_influence = { "defense": 4 }
            ),
            'sword': Item(
                name = "Sword",
                description = "+5 attack when held",
                price = 10,
                passive_influence = { "attack": 5 }
            ),
            'dagger': Item(
                name = "Dagger",
                description = "+3 attack and +2 speed when held",
                price = 7,
                passive_influence = {
                    "attack": 3,
                    "speed":  2
                }
            ),
            'greatsword': Item(
                name = "Greatsword",
                description = "+15 attack, +3 defense and -4 speed when held",
                price = 15,
                passive_influence = {
                    "attack": 15,
                    "defense": 3,
                    "speed":  -4
                }
            ),
            'obsidian-armor': Item(
                name = "Obsidian-armor",
                description = "full set of obsidian armor; +20 defense and -6 speed when held",
                price = 30,
                passive_influence = {
                    "defense": 20,
                    "speed":   -6
                }
            ),
            'potion': Item(
                name = "Potion",
                description = "+10 health when used",
                price = 3,
                active_influence = { "health": 10 },
                usages_left = 1
            ),
            'beer': Item(
                name = "Beer",
                description = "-2 speed when used",
                price = 2,
                active_influence = { "speed": -2 },
                usages_left = 1
            ),
            'coffee': Item(
                name = "Coffee",
                description = "+2 speed when used",
                price = 5,
                active_influence = { "speed": 2 },
                usages_left = 1
            ),
            'antidote': Item(
                name = "Antidote",
                description = "+6 defense when used",
                price = 15,
                active_influence = { "defense": 6 },
                usages_left = 1
            ),
            'milk': Item(
                name = "Milk",
                description = "+6 attack when used",
                price = 15,
                active_influence = { "attack": 6 },
                usages_left = 1
            )
        }

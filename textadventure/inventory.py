from .json_serialization import serializable_class, Serializable

@serializable_class
class Inventory(Serializable):
    def __init__(self, **kwargs):
        self.items = []
        self.influenced_entity = None
        super().__init__(**kwargs)

    def serialize(self):
        d = super().serialize()
        del d['influenced_entity']
        return d

    def __bool__(self):
        return bool(self.items)

    def __iter__(self):
        return self.items.__iter__()

    def add_item(self, item):
        self.items.append(item)
        if self.influenced_entity:
            return self.influenced_entity.equip(item)

    def use_item(self, item=None, index=-1):
        item.usages_left -= 1
        if item.usages_left == 0:
            item = self.__remove_item(item, index)
        if self.influenced_entity:
            return self.influenced_entity.use(item)
    
    def remove_item(self, item=None, index=-1):
        item = self.__remove_item(item, index)
        if self.influenced_entity:
            return self.influenced_entity.unequip(item)
    
    def __remove_item(self, item, index):
        if item:
            self.items.remove(item)
            return item
        else:
            return self.items.pop(index)

    def clear(self):
        items = [i for i in self.items]
        while(self.items):
            self.remove_item(index=0)
        return items

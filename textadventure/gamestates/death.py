import math

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Death(GameState):
    def run(self):
        super().run()
        self.game.player.health = 100
        inventory = self.game.player.inventory.clear()
        if self.game.bonus_tasks:
            for item in inventory:
                item.price = math.ceil(item.price / 2)
                item.quantity_left = 1
            self.game.gamestates['gravedigger'].inventory = inventory
        return []

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Quit(GameState):
    def run(self):
        super().run()
        choice = input("Save before exiting? (Y/N)")
        if choice.lower() == "y":
            # run the 'save' gamestate directly (instead of returning it) in order to not save the 'quit' gamestate (self)
            self.game.gamestates['save'].run()
        return None

import copy
import textwrap

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Shop(GameState):
    name = "shop"
    inventory = []

    def run(self):
        super().run()
        if self.inventory:
            item = None
            while True:
                print(textwrap.dedent(f"""\
                    Welcome to the {self.name}
                    You have {self.game.player.gold} gold to spend. This is what I'm selling:
                    """))
                (item, selection, _) = super().show_items_menu(
                    self.inventory,
                    action = "buy.",
                    description_generator = lambda item: f"for {str(item.price).rjust(4)} gold ({item.description})")
                if item != None:
                    break
                print(f"I do not sell '{selection}'.")
            if item:
                if self.game.player.gold >= item.price:
                    self.game.player.gold -= item.price
                    if hasattr(item, 'quantity_left'):
                        item.quantity_left -= 1
                        if item.quantity_left == 0:
                            self.inventory.remove(item)
                        item = copy.copy(item)
                        del item.quantity_left
                    else:
                        item = copy.copy(item)
                    self.game.player.inventory.add_item(item)
                    print(textwrap.dedent(f"""\
                        You have chosen {item.name}.
                        You have {self.game.player.gold} gold left."""))
                else:
                    print("Not enough gold.")
                return [self]
        else:
            print(textwrap.dedent(f"""\
                Welcome to the {self.name}
                I don't have any items for sale at the moment,
                come back later"""))
        return []

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Portal(GameState):
    def __init__(self, **kwargs):
        self.target_gamestates = []
        super().__init__(**kwargs)

    def run(self):
        super().run()
        for gamestate in self.target_gamestates:
            gamestate.game = self.game
        return self.target_gamestates
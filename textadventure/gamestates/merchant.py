import math
import textwrap

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Merchant(GameState):
    def run(self):
        super().run()
        if self.game.player.inventory:
            item = None
            index = None
            while True:
                print(textwrap.dedent(f"""\
                    Welcome to the merchant!
                    You have {self.game.player.gold} gold. This is what I would pay for your items:
                    """))
                (item, selection, index) = super().show_items_menu(
                    self.game.player.inventory,
                    action = "sell.",
                    description_generator = lambda item: f"for {str(self.__item_price(item)).rjust(4)} gold")
                if item != None:
                    break
                print(f"You do not possess a {selection}.")
            if not item:
                return []
            self.game.player.inventory.remove_item(index=index)
            self.game.player.gold += self.__item_price(item)
            print(textwrap.dedent(f"""\
                You have chosen {item.name}.
                You now have {self.game.player.gold} gold left.
                Removed item from inventory."""))
            return [self]
        else:
            print(textwrap.dedent("""\
                Sorry, you have nothing to sell.
                Thanks for visiting!"""))
            return []
    
    def __item_price(self, item):
        return math.floor(item.price / 2)

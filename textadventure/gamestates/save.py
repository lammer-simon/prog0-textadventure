from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Save(GameState):
    def run(self):
        super().run()
        self.game.save()
        print(f"Game saved to {self.game.savefile}")
        return []

import textwrap

from ..json_serialization import serializable_class
from .gamestate import GameState
from ..enemy import Enemy

@serializable_class
class Dungeon(GameState):
    enemies = {
        'rat': lambda: Enemy(
            name="Rat",
            health=30,
            attack=10,
            defense=15,
            speed=50,
            min_gold=1,
            max_gold=5
        ),
        'gnoll': lambda: Enemy(
            name="Gnoll",
            health=60,
            attack=30,
            defense=40,
            speed=20,
            min_gold=5,
            max_gold=10
        ),
        'wolf': lambda: Enemy(
            name="Wolf",
            health=40,
            attack=25,
            defense=30,
            speed=60,
            min_gold=10,
            max_gold=15
        )
    }
    description = "You see trees of green, red roses too. You see them bloom for me and you. And you think to yourself: what a wonderful world."
    entered = False

    def run(self):
        super().run()
        if not self.entered:
            self.entered = True
            self.room_number = 0
            self.room = {
                'enemies': [],
                'chest': []
            }
            self.setup_room()
            self.print_description()
        while True:
            print("What do you want to do?\n")
            result = super().show_menu([
                {
                    'key': "1",
                    'name': "Inventory",
                    'value': [self, self.game.gamestates['inventory']]
                }, {
                    'key': "2",
                    'name': "Look Around",
                    'value': self.print_description
                }, {
                    'key': "3",
                    'name': "Attack",
                    'value': self.__attack
                }, {
                    'key': "4",
                    'name': "Open chest",
                    'value': self.__chest
                }, {
                    'key': "5",
                    'name': "Move",
                    'value': self.__move
                }, {
                    'key': "0",
                    'name': "Run away (leave dungeon)",
                    'value': []
                }
            ], key_prefix="  ", key_postfix=") ")[0]
            if result == None:
                print("Invalid choice. Try again.")
            if callable(result):
                result = result()
            if isinstance(result, list):
                return result
    
    def print_description(self):
        if self.game.bonus_tasks:
            print(textwrap.dedent("""\
                ********************************************************************************
                *                    /   \              /'\       _                              *
                *\_..           /'.,/     \_         .,'   \     / \_                            *
                *    \         /            \      _/       \_  /    \     _                     *
                *     \__,.   /              \    /           \/.,   _|  _/ \                    *
                *          \_/                \  /',.,''\      \_ \_/  \/    \                   *
                *                           _  \/   /    ',../',.\    _/      \                  *
                *             /           _/m\  \  /    |         \  /.,/'\   _\                 *
                *           _/           /MMmm\  \_     |          \/      \_/  \                *
                *          /      \     |MMMMmm|   \__   \          \_       \   \_              *
                *                  \   /MMMMMMm|      \   \           \       \    \             *
                *                   \  |MMMMMMmm\      \___            \_      \_   \            *
                *                    \|MMMMMMMMmm|____.'  /\_            \       \   \_          *
                *                    /'.,___________...,,'   \            \   \        \         *
                *                   /       \          |      \    |__     \   \_       \        *
                *                 _/        |           \      \_     \     \    \       \_      *
                *                /                               \     \     \_   \        \     *
                *                                                 \     \      \   \__      \    *
                *                                                  \     \_     \     \      \   *
                *                                                   |      \     \     \      \  *
                *                                                    \ms          |            \ *
                ********************************************************************************"""))
        print(self.description)
    
    def setup_room(self):
        self.room_number += 1
        if self.room_number % 2 == 1:
            self.room['enemies'] = [
                self.enemies['rat'](),
                self.enemies['gnoll']()
            ]
            self.room['chest'] = []
        else:
            self.room['enemies'] = [
                self.enemies['wolf'](),
                self.enemies['rat']()
            ]
            self.room['chest'] = [
                self.game.items['potion']
            ]
    
    def __attack(self):
        if self.room['enemies']:
            while self.__enemies_alive():
                print("You see the following enemies:\n")
                choice = None
                while True:
                    choice = super().show_menu(
                        [
                            {
                                'key':   str(index + 1),
                                'name':  f"{enemy.name.ljust(16)}({enemy.health} HP)",
                                'value': enemy
                            } for (index, enemy) in enumerate(self.room['enemies'])
                        ],
                        key_prefix="  ",
                        key_postfix=") ",
                        prompt=textwrap.dedent(f"""
                            You have {self.game.player.health} health.
                            Which enemy would you like to attack?
                            > """))[0]
                    if choice:
                        break
                    print(f"Please input a positive integer beween 1 and {len(self.room['enemies'])}.")
                lethal_enemy = self.__attack_enemies([
                    e for e in self.room['enemies']
                        if e.speed > self.game.player.speed])
                if not lethal_enemy:
                    damage = self.game.player.damage(choice)
                    print(f"You attacked {choice.name} and dealt {damage} damage.")
                    if not choice.isalive():
                        self.game.player.gold += choice.gold
                        print(f"{choice.name} died. It dropped {choice.gold} gold.")
                        self.room['enemies'].remove(choice)
                    lethal_enemy = self.__attack_enemies([
                        e for e in self.room['enemies']
                            if e.speed <= self.game.player.speed])
                if lethal_enemy:
                    print(f"You were killed by {lethal_enemy.name}.")
                    return [self.game.gamestates['death']]
            print(textwrap.dedent("""\
                All enemies defeated.
                You are alone in this room."""))
        else:
            print("You are alone in this room.")

    def __attack_enemies(self, enemies):
        for enemy in enemies:
            damage = enemy.damage(self.game.player)
            print(f"{enemy.name} attacked you and dealt {damage} damage.")
            if not self.game.player.isalive():
                return enemy
    
    def __chest(self):
        if not self.__enemies_blocking():
            if self.room['chest']:
                for item in self.room['chest']:
                    self.room['chest'].remove(item)
                    self.game.player.inventory.add_item(item)
                    print(f"You collected {item.name} from the chest.")
            else:
                print("The chest is empty.")
    
    def __move(self):
        if not self.__enemies_blocking():
            self.setup_room()
    
    def __enemies_alive(self):
        return bool(self.room['enemies'])
    
    def __enemies_blocking(self):
        if self.__enemies_alive():
            print("Monsters are blocking your way.")
            return True
        else:
            return False

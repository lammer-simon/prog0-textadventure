import textwrap

from ..json_serialization import serializable_class
from .gamestate import GameState

@serializable_class
class Inventory(GameState):
    def run(self):
        super().run()
        if self.game.player.inventory:
            item = None
            while True:
                print(textwrap.dedent(f"""\
                    Welcome to your inventory {self.game.player.name}!
                    These are your items:
                    """))
                item = super().show_items_menu(self.game.player.inventory, action="use/drop:")[0]
                if item != None:
                    break
                print("Item does not exist.")
            if item:
                selection = input(f"Do you want to 'use' or 'drop' {item.name}? Else 'quit'.\n> ")
                if selection == "use":
                    usage_result = self.game.player.inventory.use_item(item)
                    usage_result_is_list = isinstance(usage_result, list)
                    if usage_result or usage_result_is_list:
                        print(f"You used {item.name}.")
                        for attribute, amount in item.active_influence.items():
                            if hasattr(self.game.player, attribute):
                                print(textwrap.dedent(f"""\
                                    It increased your {attribute} by {amount}.
                                    You now have {getattr(self.game.player, attribute)} {attribute}."""))
                        if usage_result_is_list:
                            return usage_result
                    else:
                        print("You cannot use this item.")
                elif selection == "drop":
                    self.game.player.inventory.remove_item(item)
                    print(f"You dropped {item.name}.")
                else:
                    print("Nothing done.")
        else:
            print("Your inventory is empty.")
        return []

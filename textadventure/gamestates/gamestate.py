from ..json_serialization import serializable_class, Serializable

class GameState(Serializable):
    def __init__(self, **kwargs):
        self.game = None
        super().__init__(**kwargs)

    def serialize(self):
        d = super().serialize()
        del d['game']
        return d

    def run(self):
        pass

    @staticmethod
    def show_menu(menu, key_prefix="", key_postfix=" ", key_padding_length=0, key_padding_character=" ", prompt="\n> "):
        for item in menu:
            if not item.get('hidden'):
                print(f"{key_prefix}{item['key'].ljust(key_padding_length, key_padding_character)}{key_postfix}{item['name']}")
        selection = input(prompt)
        for (index, item) in enumerate(menu):
            if selection == item['key']:
                return (item['value'], selection, index)
        return (None, selection, -1)
    
    @staticmethod
    def menu_with_quit(menu):
        menu.append({
            'key':    "quit",
            'name':   "",
            'value':  False,
            'hidden': True
        })
        return menu
    
    @staticmethod
    def show_items_menu(inventory, action="select:", description_generator=lambda item: f"({item.description})"):
        return GameState.show_menu(
            GameState.menu_with_quit([
                {
                    'key':   item.name,
                    'name':  description_generator(item),
                    'value': item
                } for item in inventory
            ]),
            key_prefix="  * ",
            key_padding_length=20,
            prompt=f"\nType 'quit' or the name of the item you want to {action}\n> ")

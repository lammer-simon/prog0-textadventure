import textwrap

from ..json_serialization import serializable_class
from .gamestate import GameState
from . import Dungeon

@serializable_class
class Village(GameState):
    def run(self):
        super().run()
        while True:
            if self.game.bonus_tasks:
                print(textwrap.dedent("""\
                    ************************************
                    *        _T      .,,.    ~--~ ^^   *
                    *  ^^   // \                    ~  *
                    *       ][O]    ^^      ,-~ ~      *
                    *    /''-I_I         _II____       *
                    * __/_  /   \ ______/ ''   /'\_,__ *
                    *   | II--'''' \,--:--..,_/,.-{ }, *
                    * ; '/__\,.--';|   |[] .-.| O{ _ } *
                    * :' |  | []  -|   ''--:.;[,.'\,/  *
                    * '  |[]|,.--'' '',   ''-,.    |   *
                    *   ..    ..-''    ;       ''. '   *
                    ************************************"""))
            print(textwrap.dedent(f"""\
                Welcome to Prog0 Village!
                What do you want to do?
                """))
            menu_items = [
                {
                    'key': "1",
                    'name': "Inventory",
                    'value': [self, self.game.gamestates['inventory']]
                }, {
                    'key': "2",
                    'name': "Merchant",
                    'value': [self, self.game.gamestates['merchant']]
                }, {
                    'key': "3",
                    'name': "Blacksmith",
                    'value': [self, self.game.gamestates['blacksmith']]
                }, {
                    'key': "4",
                    'name': "Druid",
                    'value': [self, self.game.gamestates['druid']]
                }, {
                    'key': "5",
                    'name': "Dungeon",
                    'value': [self, Dungeon(game=self.game)]
                }, {
                    'key': "6" if not self.game.bonus_tasks else "9",
                    'name': "Save game",
                    'value': [self, self.game.gamestates['save']]
                }, {
                    'key': "0",
                    'name': "Quit game",
                    'value': [self, self.game.gamestates['quit']]
                }
            ]
            if self.game.bonus_tasks:
                menu_items[-2:-2] = [
                    {
                        'key': "6",
                        'name': "Gravedigger",
                        'value': [self, self.game.gamestates['gravedigger']]
                    }
                ]
            if self.game.next_gamestates:
                menu_items[-2:-2] = [
                    {
                        'key': "9" if not self.game.bonus_tasks else "8",
                        'name': "Exit Village",
                        'value': []
                    }
                ]
            result = super().show_menu(
                menu_items,
                key_prefix="  ",
                key_postfix=") "
            )[0]
            if isinstance(result, list):
                return result
            else:
                print("Invalid choice. Try again.")

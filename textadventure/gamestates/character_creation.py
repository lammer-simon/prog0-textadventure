import textwrap

from ..json_serialization import serializable_class
from ..player import Player
from .gamestate import GameState

@serializable_class
class CharacterCreation(GameState):
    def run(self):
        super().run()
        while True:
            print("Welcome to P0 Dungeon Quest character creator!")
            name = input("Enter your name: ")
            attack = 0
            defense = 0
            speed = 0
            while True:
                print(textwrap.dedent(f"""\
                    You have {self.game.INITIAL_STATUS_POINTS} points to assign to your character.
                    Start now to assign those Points to your characters attack, defense and speed."""))
                attack = self.input_stat("Attack: ")
                defense = self.input_stat("Defense: ")
                speed = self.input_stat("Speed: ")
                if (attack + defense + speed) <= self.game.INITIAL_STATUS_POINTS:
                    break
                print(f"Sorry, it seems like you spent more than {self.game.INITIAL_STATUS_POINTS} ability points on your character... Try that again!")
            player = Player(
                name=name,
                attack=attack,
                defense=defense,
                speed=speed
            )
            print("Before you store your character please confirm your stats!")
            player.print_stats()
            confirmation = ""
            while not confirmation in ["y", "n"]:
                confirmation = input("Is this correct? (Y/N) ").lower()
            if confirmation == "y":
                self.game.player = player
                return [self.game.gamestates['village']]

    @staticmethod
    def input_stat(message):
        while(True):
            val = input(message)
            try:
                val_int = int(val)
                if val_int > 0:
                    return val_int
            except:
                pass
            print("Please input a positive integer.")

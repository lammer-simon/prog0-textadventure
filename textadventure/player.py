from .entity import Entity
from .json_serialization import serializable_class, Serializable
from .inventory import Inventory

@serializable_class
class Player(Entity):
    def __init__(self, **kwargs):
        self.gold = 100
        self.inventory = Inventory()
        super().__init__(**kwargs)
        self.inventory.influenced_entity = self

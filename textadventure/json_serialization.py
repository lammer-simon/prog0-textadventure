import json

serializable_classes = []

def serializable_class(klass):
    serializable_classes.append(klass)
    return klass

class Serializable:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def serialize(self):
        return self.__dict__.copy()

def load_object(filename):
    with open(filename, "r") as f:
        return json.load(f, cls=CustomDecoder)

def save_object(obj, filename):
    with open(filename, "w") as f:
        json.dump(obj, f, cls=CustomEncoder, indent=2)

class CustomDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if '__class__' not in obj:
            return obj
        type = obj['__class__']

        for klass in serializable_classes:
            if type == klass.__name__:
                return klass(**obj)
        return obj

class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        for klass in serializable_classes:
            if isinstance(obj, klass):
                cpy = obj.serialize()
                cpy["__class__"] = obj.__class__.__name__
                return cpy
        return json.JSONEncoder.default(self, obj)

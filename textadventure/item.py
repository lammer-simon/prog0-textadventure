import sys

from .json_serialization import serializable_class, Serializable

@serializable_class
class Item(Serializable):
    def __init__(self, **kwargs):
        self.name                   = ""
        self.description            = ""
        self.price                  = 0
        self.passive_influence      = {}
        self.active_influence       = {}
        self.usages_left            = 0 # 0 = infinite usages
        self.gamestates_after_usage = None
        super().__init__(**kwargs)
